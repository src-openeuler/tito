%global use_python3 1
%global use_python2 0
%global ourpythonbin %{__python3}
%global our_sitelib %{python3_sitelib}
%{!?our_sitelib: %define our_sitelib %(%{ourpythonbin} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name: tito
Version: 0.6.21
Release: 1%{?dist}
Summary: A tool for managing rpm based git projects

License: GPLv2
URL: https://github.com/rpm-software-management/tito
# Sources can be obtained by
# git clone https://github.com/rpm-software-management/tito
# cd tito
# tito build --tgz
Source0: %{name}-%{version}.tar.gz

BuildArch: noarch

BuildRequires: python3-devel
BuildRequires: python3-setuptools
Requires: python3-setuptools
#Requires: python3-bugzilla
Requires: python3-blessed
Requires: rpm-python3

BuildRequires: asciidoc
BuildRequires: docbook-style-xsl
BuildRequires: libxslt
BuildRequires: rpmdevtools
BuildRequires: rpm-build
BuildRequires: tar
BuildRequires: which
BuildRequires: git

Requires: rpm-build
Requires: rpmlint
Requires: rpmdevtools
# Cheetah used not to exist for Python 3, but it's what Mead uses.  We
# install it and call via the command line instead of importing the
# previously potentially incompatible code, as we have not yet got
# around to changing this
Requires: /usr/bin/cheetah

%description
Tito is a tool for managing tarballs, rpms, and builds for projects using
git.

%prep
%setup -q -n tito-%{version}

%build
%{ourpythonbin} setup.py build
# convert manages
a2x --no-xmllint -d manpage -f manpage titorc.5.asciidoc
a2x --no-xmllint -d manpage -f manpage tito.8.asciidoc
a2x --no-xmllint -d manpage -f manpage tito.props.5.asciidoc
a2x --no-xmllint -d manpage -f manpage releasers.conf.5.asciidoc

%install
rm -rf $RPM_BUILD_ROOT
%{ourpythonbin} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{our_sitelib}/*egg-info/requires.txt
# manpages
%{__mkdir_p} %{buildroot}%{_mandir}/man5
%{__mkdir_p} %{buildroot}%{_mandir}/man8
cp -a titorc.5 tito.props.5 releasers.conf.5 %{buildroot}/%{_mandir}/man5/
cp -a tito.8 %{buildroot}/%{_mandir}/man8/
# bash completion facilities
install -Dp -m 0644 share/tito_completion.sh %{buildroot}%{_datadir}/bash-completion/completions/tito


%files
%doc AUTHORS COPYING
%doc doc/*
%doc %{_mandir}/man5/titorc.5*
%doc %{_mandir}/man5/tito.props.5*
%doc %{_mandir}/man5/releasers.conf.5*
%doc %{_mandir}/man8/tito.8*
%{_bindir}/tito
%{_bindir}/generate-patches.pl
%{_datadir}/bash-completion/completions/tito
%dir %{our_sitelib}/tito
%{our_sitelib}/tito/*
%{our_sitelib}/tito-*.egg-info


%changelog
* Mon Aug 15 2022 hanjinpeng <hanjinpeng@kylinos.cn> - 0.6.21-1
- Package init
